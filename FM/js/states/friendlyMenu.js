var formationCount = 0;

var friendlyMenuState = {

    menuOptionWidth : 400,
    menuOptionScale : 0.5,
    /*******************************************
     *           AUXILIARY FUNCTIONS           *
     *******************************************/
    startMatch: function(data) {

        var matchID = data.matchID;
        console.log(data);

        var side = data.side;

        var infoTeam = {
            matchID: matchID,
            side: side,
            color: game.teamColor,
            name: game.teamName,
            formation: formationCount
        };

        console.log("friendlyMenuState - info sending");
        Client.sendInfoTeam(infoTeam);
    },

    getOppInfo: function(data) {
        game.global.selectedFormation = formationCount;

        game.global.rivalSelectedFormation = game.global.formations[data.formation];
        game.global.rivalSide = data.side;
        game.global.rivalName = data.name;
        game.global.rivalColor = data.color;

        game.teamAI = 'fox';
        game.rivalAI = 'fox';
        game.control = 'simulate';
        game.state.start('playOnline');
    },


    /*******************************************
     *            PHASER FUNCTIONS             *
     *******************************************/

    /* *****************************************
     * preload
     *
     * Set counters for UI building, storage
     * arrays and default choices.
     * *****************************************/
    preload: function () {
        this.optionCount = 1;
        this.countX = 0;
        this.countY = 0;
        this.colorCount = 0;
        this.aiCount = 0;
        this.controlCount = 0;
    },

    /* *****************************************
     * create
     *
     * Create the menu options and add the
     * callback functions.
     * *****************************************/
    create: function() {
        // Add a background image
        game.add.image(0, 55, game.worldLeague);

        var bg = game.add.image(505, game.world.centerY + 25, 'shadowBG');
        bg.anchor.setTo(0.5, 0.5);
        bg.scale.setTo(0.97, 1.5);

        // Create header using UI.js
        createHeader();

        // Create lateral menu using UI.js
        createLateralMenu(game, this);

        // Button to start the game
        var playButton = game.add.image(game.canvas.width - 120, 7, 'play');
        playButton.scale.setTo(0.5);
        playButton.inputEnabled = true;
        playButton.input.useHandCursor = true;
        playButton.events.onInputUp.add(function (target) {
            Client.sendTest();
            Client.joinQueue();

            // Waiting in queue animation

        }, this);

        // Field background for the formations
        var field = game.add.image(game.world.centerX + 220, 310, 'field');
        field.anchor.setTo(0.5,0.5);
        field.angle = 90;
        field.scale.setTo(0.7);

        // Formation overlay selection
        this.formation = game.add.image(game.world.centerX + 220, 310, game.global.formations[formationCount]);
        this.formation.anchor.setTo(0.5,0.5);
        this.formation.scale.setTo(0.7);

        // Font style for formation name and navigators
        var styleFormation = {font: '28pt PopWarner', fill: game.global.colors.ORANGE,
            stroke: game.global.colors.DARKGRAY, strokeThickness: 4};

        // Formation name label
        this.fieldText = game.add.text(game.world.centerX + 220, 120, game.global.formations[formationCount], styleFormation);
        this.fieldText.anchor.setTo(0.5,0.5);
        this.fieldText.anchor.x = Math.round(this.fieldText.width * 0.5) / this.fieldText.width;

        // Formation change inputs
        // - Left arrow
        var leftArrow = game.add.text(game.world.centerX + 100, 120, '<', styleFormation);
        leftArrow.anchor.setTo(0,0.5);
        leftArrow.inputEnabled = true;
        leftArrow.input.useHandCursor = true;
        leftArrow.events.onInputUp.add(function (target) {
            formationCount--;
            if (formationCount < 0) {
                formationCount = game.global.formations.length - 1;
            }
            this.formation.loadTexture(game.global.formations[formationCount]);
            this.fieldText.text =  game.global.formations[formationCount];
        }, this);
        // - Right arrow
        var rightArrow = game.add.text(game.world.centerX + 340, 120, '>', styleFormation);
        rightArrow.anchor.setTo(1,0.5);
        rightArrow.inputEnabled = true;
        rightArrow.input.useHandCursor = true;
        rightArrow.events.onInputUp.add(function (target) {
            formationCount++;
            if (formationCount >= game.global.formations.length) {
                formationCount = 0;
            }
            this.formation.loadTexture(game.global.formations[formationCount]);
            this.fieldText.text =  game.global.formations[formationCount];
        }, this);

        // Text for next match rival
        var style = {font: '22pt PopWarner', fill: game.global.colors.ORANGE,
            stroke: game.global.colors.DARKGRAY, strokeThickness: 4};

        var teamName = game.add.text(game.world.centerX - 50, 300, game.teamName, style);
        teamName.anchor.setTo(0.5,0.5);
        teamName.anchor.x = Math.round(teamName.width * 0.5) / teamName.width;

    }
};