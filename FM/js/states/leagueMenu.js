var formationCount = 0;

var leagueMenuState = {

    menuOptionWidth: 400,
    menuOptionScale: 0.5,
    /*******************************************
     *           AUXILIARY FUNCTIONS           *
     *******************************************/

    /* *****************************************
     * addAI
     *
     * Factory for the easy creation of AI
     * symbols.
     * *****************************************/
    addAI: function (text) {
        var img;

        // Default AI symbol is selected
        if (text == 'fox')
            img = text + '_hover';
        else
            img = text;

        var icon = game.add.image((this.aiCount * 40) + game.world.centerX + 240, 510, img);
        icon.anchor.setTo(0.5, 0.5);
        icon.scale.setTo(0.22, 0.22);
        icon.inputEnabled = true;
        icon.input.useHandCursor = true;

        // Save AI when clicking the symbol
        icon.events.onInputUp.add(function (target) {
            for (var i = 0; i < this.aiIcons.length; i++) {
                this.aiIcons[i].loadTexture(game.global.animals[i]);
            }
            target.loadTexture(text + "_hover");
            this.selectedAI = text;
        }, this);
        this.aiIcons.push(icon);

        this.aiCount++;
    },

    /* *****************************************
     * showAIs
     *
     * Function that adds the AI selector
     * *****************************************/
    showAIs: function () {
        this.addAI('fox');
        this.addAI('donkey');
    },

    /* *****************************************
     * addControl
     *
     * Factory for the easy creation of control
     * symbols.
     * *****************************************/
    addControl: function (text) {
        var img;

        // Default control symbol is selected
        if (text == 'simulate')
            img = text + '_hover';
        else
            img = text;

        var icon = game.add.image((this.controlCount * 60) + game.world.centerX - 30, 510, img);
        icon.anchor.setTo(0.5, 0.5);
        icon.scale.setTo(0.3, 0.3);
        icon.inputEnabled = true;
        icon.input.useHandCursor = true;

        // Save control when clicking the symbol
        icon.events.onInputUp.add(function (target) {
            for (var i = 0; i < this.controlIcons.length; i++) {
                this.controlIcons[i].loadTexture(game.global.controls[i]);
            }
            target.loadTexture(text + "_hover");
            this.selectedControl = text;
        }, this);
        this.controlIcons.push(icon);

        this.controlCount++;
    },

    /* *****************************************
     * showControls
     *
     * Function that adds the control selector
     * *****************************************/
    showControls: function () {
        this.addControl('simulate');
        this.addControl('controller');
    },

    /*******************************************
     *            PHASER FUNCTIONS             *
     *******************************************/

    /* *****************************************
     * preload
     *
     * Set counters for UI building, storage
     * arrays and default choices.
     * *****************************************/
    preload: function () {
        this.optionCount = 1;
        this.countX = 0;
        this.countY = 0;
        this.colorCount = 0;
        this.aiCount = 0;
        this.controlCount = 0;

        this.colorBalls = new Array();
        this.aiIcons = new Array();
        this.controlIcons = new Array();
        this.selectedColor = 'red';
        this.selectedAI = 'fox';
        this.selectedControl = 'simulate';
    },

    /* *****************************************
     * create
     *
     * Create the menu options and add the
     * callback functions.
     * *****************************************/
    create: function () {
        // Add a background image
        var bgimage = game.add.image(0, 0, 'background');
        bgimage.scale.setTo(1.75, 1.75);

        var bg = game.add.image(game.world.centerX, game.world.centerY + 25, 'shadowBG');
        bg.anchor.setTo(0.5, 0.5);
        bg.scale.setTo(1.33, 1.40);

        // Create header using UI.js
        createHeader();

        // Button to start the game
        var playButton = game.add.image(game.canvas.width - 120, 7, 'play');
        playButton.scale.setTo(0.5);
        playButton.inputEnabled = true;
        playButton.input.useHandCursor = true;
        playButton.events.onInputUp.add(function (target) {
            game.league.nextMatch();
            game.global.selectedFormation = formationCount;
            game.teamAI = this.selectedAI;
            game.rivalAI = 'fox';
            game.control = this.selectedControl;
            game.isFriendly = false;
            game.state.start('play');
        }, this);

        var attributes = ["Celerity", "Resistance", "Strength", "Defense", "Technique"];
        var markStyle = {
            font: '22pt PopWarner', fill: game.global.colors.WHITE,
            stroke: game.global.colors.DARKGRAY, strokeThickness: 4
        };
        var displaceY = 80;
        this.maxPoints = 20;
        var pointsText = game.add.text(30, game.world.centerY - displaceY - 50, "Points:", {
            font: '25pt PopWarner', fill: game.global.colors.WHITE,
            stroke: game.global.colors.DARKGRAY, strokeThickness: 4
        });
        pointsText.anchor.setTo(0,0.5);
        this.pointsAttrib = game.add.text(210, game.world.centerY - displaceY - 50, this.maxPoints, {
            font: '25pt PopWarner', fill: game.global.colors.WHITE,
            stroke: game.global.colors.DARKGRAY, strokeThickness: 4
        });
        this.pointsAttrib.anchor.setTo(1,0.5);
        this.remainingPoints = this.maxPoints;
        for (var i = 0; i < 5; i++) {

            // Creation of slide bar and markers
            var bar = game.add.image(game.world.centerX - 80, game.world.centerY - displaceY + (55 * i), 'slide_bar');
            bar.anchor.setTo(0.5, 0.5);
            var selector = game.add.image(game.world.centerX - 80 - bar.width / 2 + 12, game.world.centerY - displaceY + (55 * i), 'slide_select');
            selector.anchor.setTo(0.5, 0.5);
            switch (i) {
                case 0:
                    this.celeritySelector = selector;
                    this.celerityMarker = 1;
                    this.celerityMarkerText = game.add.text(210, game.world.centerY - displaceY + (55 * i), this.celerityMarker, markStyle);
                    this.celerityMarkerText.anchor.setTo(1,0.5);
                    this.celerityText = game.add.text(30, game.world.centerY - displaceY + (55 * i), attributes[i], markStyle);
                    this.celerityText.anchor.setTo(0,0.5);
                    break;
                case 1:
                    this.resistanceSelector = selector;
                    this.resistanceMarker = 1;
                    this.resistanceMarkerText = game.add.text(210, game.world.centerY - displaceY + (55 * i), this.resistanceMarker, markStyle);
                    this.resistanceMarkerText.anchor.setTo(1,0.5);
                    this.resistanceText = game.add.text(30, game.world.centerY - displaceY + (55 * i), attributes[i], markStyle);
                    this.resistanceText.anchor.setTo(0,0.5);
                    break;
                case 2:
                    this.strengthSelector = selector;
                    this.strengthMarker = 1;
                    this.strengthMarkerText = game.add.text(210, game.world.centerY - displaceY + (55 * i), this.strengthMarker, markStyle);
                    this.strengthMarkerText.anchor.setTo(1,0.5);
                    this.strengthText = game.add.text(30, game.world.centerY - displaceY + (55 * i), attributes[i], markStyle);
                    this.strengthText.anchor.setTo(0,0.5);
                    break;
                case 3:
                    this.defenseSelector = selector;
                    this.defenseMarker = 1;
                    this.defenseMarkerText = game.add.text(210, game.world.centerY - displaceY + (55 * i), this.defenseMarker, markStyle);
                    this.defenseMarkerText.anchor.setTo(1,0.5);
                    this.defenseText = game.add.text(30, game.world.centerY - displaceY + (55 * i), attributes[i], markStyle);
                    this.defenseText.anchor.setTo(0,0.5);
                    break;
                case 4:
                    this.techniqueSelector = selector;
                    this.techniqueMarker = 1;
                    this.techniqueMarkerText = game.add.text(210, game.world.centerY - displaceY + (55 * i), this.techniqueMarker, markStyle);
                    this.techniqueMarkerText.anchor.setTo(1,0.5);
                    this.techniqueText = game.add.text(30, game.world.centerY - displaceY + (55 * i), attributes[i], markStyle);
                    this.techniqueText.anchor.setTo(0,0.5);
                    break;
                default:
                    break;
            }

            // Creation of +- buttons
            var plus = game.add.image(game.world.centerX + bar.width / 2 - 60, game.world.centerY - displaceY - 10 + (55 * i), 'plus');
            plus.anchor.setTo(0.5, 0.5);
            plus.inputEnabled = true;
            plus.input.useHandCursor = true;
            var minus = game.add.image(game.world.centerX + bar.width / 2 - 60, game.world.centerY - displaceY + 10 + (55 * i), 'minus');
            minus.anchor.setTo(0.5, 0.5);
            minus.inputEnabled = true;
            minus.input.useHandCursor = true;

            // Button input assignment
            switch (i) {
                case 0:
                    plus.events.onInputUp.add(function (target) {
                        if (this.celerityMarker < 10 && this.remainingPoints > 0) {
                            this.celerityMarker++;
                            this.remainingPoints--;
                            this.pointsAttrib.text = this.remainingPoints;
                            this.celeritySelector.x = this.celeritySelector.x + 20;
                            this.celerityMarkerText.text = this.celerityMarker;
                        }
                    }, this);
                    minus.events.onInputUp.add(function (target) {
                        if (this.celerityMarker > 1 && this.remainingPoints < this.maxPoints) {
                            this.celerityMarker--;
                            this.remainingPoints++;
                            this.pointsAttrib.text = this.remainingPoints;
                            this.celeritySelector.x = this.celeritySelector.x - 20;
                            this.celerityMarkerText.text = this.celerityMarker;
                        }
                    }, this);
                    break;
                case 1:
                    plus.events.onInputUp.add(function (target) {
                        if (this.resistanceMarker < 10 && this.remainingPoints > 0) {
                            this.resistanceMarker++;
                            this.remainingPoints--;
                            this.pointsAttrib.text = this.remainingPoints;
                            this.resistanceSelector.x = this.resistanceSelector.x + 20;
                            this.resistanceMarkerText.text = this.resistanceMarker;
                        }
                    }, this);
                    minus.events.onInputUp.add(function (target) {
                        if (this.resistanceMarker > 1 && this.remainingPoints < this.maxPoints) {
                            this.resistanceMarker--;
                            this.remainingPoints++;
                            this.pointsAttrib.text = this.remainingPoints;
                            this.resistanceSelector.x = this.resistanceSelector.x - 20;
                            this.resistanceMarkerText.text = this.resistanceMarker;
                        }
                    }, this);
                    break;
                case 2:
                    plus.events.onInputUp.add(function (target) {
                        if (this.strengthMarker < 10 && this.remainingPoints > 0) {
                            this.strengthMarker++;
                            this.remainingPoints--;
                            this.pointsAttrib.text = this.remainingPoints;
                            this.strengthSelector.x = this.strengthSelector.x + 20;
                            this.strengthMarkerText.text = this.strengthMarker;
                        }
                    }, this);
                    minus.events.onInputUp.add(function (target) {
                        if (this.strengthMarker > 1 && this.remainingPoints < this.maxPoints) {
                            this.strengthMarker--;
                            this.remainingPoints++;
                            this.pointsAttrib.text = this.remainingPoints;
                            this.strengthSelector.x = this.strengthSelector.x - 20;
                            this.strengthMarkerText.text = this.strengthMarker;
                        }
                    }, this);
                    break;
                case 3:
                    plus.events.onInputUp.add(function (target) {
                        if (this.defenseMarker < 10 && this.remainingPoints > 0) {
                            this.defenseMarker++;
                            this.remainingPoints--;
                            this.pointsAttrib.text = this.remainingPoints;
                            this.defenseSelector.x = this.defenseSelector.x + 20;
                            this.defenseMarkerText.text = this.defenseMarker;
                        }
                    }, this);
                    minus.events.onInputUp.add(function (target) {
                        if (this.defenseMarker > 1 && this.remainingPoints < this.maxPoints) {
                            this.defenseMarker--;
                            this.remainingPoints++;
                            this.pointsAttrib.text = this.remainingPoints;
                            this.defenseSelector.x = this.defenseSelector.x - 20;
                            this.defenseMarkerText.text = this.defenseMarker;
                        }
                    }, this);
                    break;
                case 4:
                    plus.events.onInputUp.add(function (target) {
                        if (this.techniqueMarker < 10 && this.remainingPoints > 0) {
                            this.techniqueMarker++;
                            this.remainingPoints--;
                            this.pointsAttrib.text = this.remainingPoints;
                            this.techniqueSelector.x = this.techniqueSelector.x + 20;
                            this.techniqueMarkerText.text = this.techniqueMarker;
                        }
                    }, this);
                    minus.events.onInputUp.add(function (target) {
                        if (this.techniqueMarker > 1 && this.remainingPoints < this.maxPoints) {
                            this.techniqueMarker--;
                            this.remainingPoints++;
                            this.pointsAttrib.text = this.remainingPoints;
                            this.techniqueSelector.x = this.techniqueSelector.x - 20;
                            this.techniqueMarkerText.text = this.techniqueMarker;
                        }
                    }, this);
                    break;
                default:
                    break;
            }
        }


        // Field background for the formations
        var field = game.add.image(game.world.centerX + 220, 330, 'field');
        field.anchor.setTo(0.5, 0.5);
        field.angle = 90;
        field.scale.setTo(0.7);

        // Formation overlay selection
        this.formation = game.add.image(game.world.centerX + 220, 330, game.global.formations[formationCount]);
        this.formation.anchor.setTo(0.5, 0.5);
        this.formation.scale.setTo(0.7);

        // Font style for formation name and navigators
        var styleFormation = {
            font: '28pt PopWarner', fill: game.global.colors.ORANGE,
            stroke: game.global.colors.DARKGRAY, strokeThickness: 4
        };

        // Formation name label
        this.fieldText = game.add.text(game.world.centerX + 220, 140, game.global.formations[formationCount], styleFormation);
        this.fieldText.anchor.setTo(0.5, 0.5);
        this.fieldText.anchor.x = Math.round(this.fieldText.width * 0.5) / this.fieldText.width;

        // Formation change inputs
        // - Left arrow
        var leftArrow = game.add.text(game.world.centerX + 100, 140, '<', styleFormation);
        leftArrow.anchor.setTo(0, 0.5);
        leftArrow.inputEnabled = true;
        leftArrow.input.useHandCursor = true;
        leftArrow.events.onInputUp.add(function (target) {
            formationCount--;
            if (formationCount < 0) {
                formationCount = game.global.formations.length - 1;
            }
            this.formation.loadTexture(game.global.formations[formationCount]);
            this.fieldText.text = game.global.formations[formationCount];
        }, this);
        // - Right arrow
        var rightArrow = game.add.text(game.world.centerX + 340, 140, '>', styleFormation);
        rightArrow.anchor.setTo(1, 0.5);
        rightArrow.inputEnabled = true;
        rightArrow.input.useHandCursor = true;
        rightArrow.events.onInputUp.add(function (target) {
            formationCount++;
            if (formationCount >= game.global.formations.length) {
                formationCount = 0;
            }
            this.formation.loadTexture(game.global.formations[formationCount]);
            this.fieldText.text = game.global.formations[formationCount];
        }, this);

        // Player label
        var playerText = game.add.text(game.world.centerX - 160, 510, game.global.selectorLabel.PLAYER,
            {
                font: '20pt PopWarner', fill: game.global.colors.ORANGE,
                stroke: game.global.colors.DARKGRAY, strokeThickness: 4
            });
        playerText.anchor.setTo(0, 0.5);
        this.showControls();
    }
};