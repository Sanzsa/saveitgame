var formationCount = 0;
var space = 'space';

var teamCreateState = {

    /*******************************************
     *           AUXILIARY FUNCTIONS           *
     *******************************************/

    /* *****************************************
     * addLeagueOption
     *
     * Factory for the easy creation of menu
     * options.
     * *****************************************/
    addLeagueOption: function (text, id, font) {
        var optionStyle = null;

        // Font size adjustment
        if (font == 'LifeCraft') {
            optionStyle = {font: '20pt ' + font, fill: game.global.colors.LIGHTBLUE, align: 'left'};
        } else if (font == 'Planewalker') {
            optionStyle = {font: '20pt ' + font, fill: game.global.colors.LIGHTBLUE, align: 'left'};
        } else if (font == 'HarryP') {
            optionStyle = {font: '24pt ' + font, fill: game.global.colors.LIGHTBLUE, align: 'left'};
        } else {
            optionStyle = {font: '15pt ' + font, fill: game.global.colors.LIGHTBLUE, align: 'left'};
        }
        var txt = game.add.text(game.world.centerX + 30, (this.optionCount * 35) + 255, text, optionStyle);
        txt.anchor.setTo(0, 0.5);
        txt.anchor.y = Math.round(txt.height * 0.5) / txt.height;

        var onOver = function (target) {
            target.fill = game.global.colors.DARKBLUE;
        };
        var onOut = function (target) {
            target.fill = game.global.colors.LIGHTBLUE;
        };
        txt.inputEnabled = true;
        txt.input.useHandCursor = true;
        txt.events.onInputUp.add(function (target) {
            game.worldLeague = id;
            this.leagueMarker.y = target.y;
            this.leagueMarker.style = target.style;
            switch (font) {
                // Horde banner
                case 'LifeCraft':
                    this.leagueMarker.text = '[';
                    break;
                // Rebel symbol
                case 'StarJedi':
                    this.leagueMarker.text = '$';
                    break;
                // Cross
                case 'Planewalker':
                    this.leagueMarker.text = '+';
                    break;
                // Lightning
                case 'HarryP':
                    this.leagueMarker.text = '\\';
                    break;
            }
        }, this);
        txt.events.onInputOver.add(onOver);
        txt.events.onInputOut.add(onOut);
        this.optionCount++;
    },

    /* *****************************************
     * addKeyboardKey
     *
     * Factory for the easy creation of keys
     * to write the team name.
     * *****************************************/
    addKeyboardKey: function (text) {
        var optionStyle = {
            font: '18pt PopWarner', fill: game.global.colors.DARKGRAY, align: 'left',
            stroke: game.global.colors.ORANGE, strokeThickness: 4
        };

        var txt;
        if (text == space) {
            txt = game.add.text((this.countX * 30) + game.world.centerX - 230, (this.countY * 30) + 300, text, {
                font: '14pt PopWarner', fill: game.global.colors.DARKGRAY, align: 'left',
                stroke: game.global.colors.ORANGE, strokeThickness: 3
            });
            txt.anchor.setTo(0.2, 0.5);
        } else {
            txt = game.add.text((this.countX * 30) + game.world.centerX - 230, (this.countY * 30) + 300, text, optionStyle);
            txt.anchor.setTo(0.5, 0.5);
            txt.anchor.x = Math.round(txt.width * 0.5) / txt.width;
        }

        var onOver = function (target) {
            target.fill = game.global.colors.ORANGE;
            target.stroke = game.global.colors.DARKGRAY;
        };
        var onOut = function (target) {
            target.fill = game.global.colors.DARKGRAY;
            target.stroke = game.global.colors.ORANGE;
        };
        txt.inputEnabled = true;
        txt.input.useHandCursor = true;
        txt.events.onInputUp.add(function (target) {
            if (this.nameInput.text.length <= game.global.maxTeamLength) {
                if (text == space)
                    this.nameInput.text = this.nameInput.text + ' ';
                else
                    this.nameInput.text = this.nameInput.text + target.text;
            }
        }, this);
        txt.events.onInputOver.add(onOver);
        txt.events.onInputOut.add(onOut);
        this.countX++;
        if (this.countX > 6) {
            this.countX = 0;
            this.countY++;
        }
    },

    /* *****************************************
     * addColor
     *
     * Factory for the easy creation of color
     * options.
     * *****************************************/
    addColor: function (text) {
        var icon = game.add.image((this.colorCount * 52) + game.world.centerX + 60, 245, text + "ball");
        icon.anchor.setTo(0.5, 0.5);

        var onOver = function (target) {
            target.loadTexture(text + "ball_hover");
        };
        var onOut = function (target) {
            target.loadTexture(text + "ball");
        };
        icon.inputEnabled = true;
        icon.input.useHandCursor = true;
        icon.events.onInputUp.add(function (target) {
            this.selectedColor = text;
            this.colorMarker.x = target.x;
        }, this);
        icon.events.onInputOver.add(onOver);
        icon.events.onInputOut.add(onOut);

        this.colorBalls.push(icon);
        this.colorCount++;
    },

    /* *****************************************
     * showColors
     *
     * Function that adds the color selector
     * *****************************************/
    showColors: function () {
        for (var i = 0; i < game.global.shirtColors.length; i++) {
            this.addColor(game.global.shirtColors[i]);
        }
    },

    /*******************************************
     *            PHASER FUNCTIONS             *
     *******************************************/

    /* *****************************************
     * preload
     *
     * Set optionCount to 1.
     * *****************************************/
    preload: function () {
        this.optionCount = 1;
        this.countX = 0;
        this.countY = 0;
        this.colorCount = 0;

        this.colorBalls = new Array();
        this.selectedColor = 'red';
        game.worldLeague = 'azeroth';
    },

    /* *****************************************
     * create
     *
     * Create the menu options and add the
     * callback functions.
     * *****************************************/
    create: function () {
        // Add a background image
        var bgimage = game.add.image(0, 0, 'background');
        bgimage.scale.setTo(1.75, 1.75);
        // Header
        createHeader();

        var bg = game.add.image(game.world.centerX, game.world.centerY + 25, 'shadowBG');
        bg.anchor.setTo(0.5, 0.5);

        // Name label
        var nameText = game.add.text(game.world.centerX - 140, 200, game.global.selectorLabel.NAME,
            {
                font: '32pt PopWarner', fill: game.global.colors.ORANGE,
                stroke: game.global.colors.DARKGRAY, strokeThickness: 4
            });
        nameText.anchor.setTo(0.5, 0.5);
        nameText.anchor.x = Math.round(nameText.width * 0.5) / nameText.width;

        // Input background for team name
        var inputBG = game.add.image(game.world.centerX - 160, 250, 'inputBG');
        inputBG.anchor.setTo(0.5, 0.5);

        // Dummy text where team name will be written
        this.nameInput = game.add.text(game.world.centerX - 160, 253, '',
            {font: '18pt PopWarner', fill: game.global.colors.LIGHTBLUE});
        this.nameInput.anchor.setTo(0.5, 0.5);

        // Create the keyboard to type the team name
        for (var i = 65; i < 91; i++) {
            this.addKeyboardKey(String.fromCharCode(i));
        }
        this.addKeyboardKey(space);

        // Erase button
        this.erase = game.add.image(game.world.centerX - 65,
            250, 'erase');
        this.erase.anchor.setTo(0.5, 0.5);
        this.erase.inputEnabled = true;
        this.erase.input.useHandCursor = true;
        this.erase.events.onInputUp.add(function (target) {
            this.nameInput.text = this.nameInput.text.substring(0, this.nameInput.text.length - 1);
        }, this);
        this.erase.events.onInputOver.add(function (target) {
            target.loadTexture('erase_orange');
        });
        this.erase.events.onInputOut.add(function (target) {
            target.loadTexture('erase');
        });

        // Color label
        var colorText = game.add.text(game.world.centerX + 140, 200, game.global.selectorLabel.COLOR,
            {
                font: '32pt PopWarner', fill: game.global.colors.ORANGE,
                stroke: game.global.colors.DARKGRAY, strokeThickness: 4
            });
        colorText.anchor.setTo(0.5, 0.5);
        colorText.anchor.x = Math.round(colorText.width * 0.5) / colorText.width;

        // Create the marker for current selected color
        this.colorMarker = game.add.image(game.world.centerX + 60, 245, 'selector');
        this.colorMarker.anchor.setTo(0.5, 0.5);

        // Creation of color balls
        this.showColors();

        this.leagueMarker = game.add.text(game.world.centerX + 15, 290, '[',
            {font: '20pt LifeCraft', fill: game.global.colors.DARKBLUE, align: 'left'});
        this.leagueMarker.anchor.setTo(0.5);
        this.leagueMarker.anchor.x = Math.round(this.leagueMarker.width * 0.5) / this.leagueMarker.width;
        this.addLeagueOption("Azeroth Premier League", "azeroth", "LifeCraft");
        this.addLeagueOption("The Elder Soccer", "nirn", "Planewalker");
        this.addLeagueOption("Star Soccer", "starWars", "StarJedi");
        this.addLeagueOption("Wizard League", "harryPotter", "HarryP");

        var onOver = function (target) {
            target.fill = game.global.colors.ORANGE;
            target.stroke = game.global.colors.DARKGRAY;
        };
        var onOut = function (target) {
            target.fill = game.global.colors.DARKGRAY;
            target.stroke = game.global.colors.ORANGE;
        };
        var optionStyle = {
            font: '22pt PopWarner', fill: game.global.colors.DARKGRAY, align: 'left',
            stroke: game.global.colors.ORANGE, strokeThickness: 3
        };

        // Button to start the game
        var playButton = game.add.text(game.world.centerX + 80, 440, game.global.optionsLabel.OK, optionStyle);
        playButton.anchor.setTo(0.5, 0.5);
        playButton.anchor.x = Math.round(playButton.width * 0.5) / playButton.width;
        playButton.inputEnabled = true;
        playButton.input.useHandCursor = true;
        playButton.events.onInputUp.add(function (target) {
            game.global.selectedFormation = formationCount;
            game.teamName = this.nameInput.text;
            if (!game.teamName) {
                game.teamName = 'YOUR TEAM';
            }
            game.teamName = game.teamName.toLowerCase();
            game.teamName = game.teamName.replace(/\b\w/g, function($1){ return $1.toUpperCase() });
            game.teamColor = this.selectedColor;
            game.league = new League();
            game.league.buildLeague();
            game.playerTeam = new Team(null, null, game.teamColor, game.teamName, game.global.selectedFormation,
                null, null);

            game.league.teams.splice(0, 0, game.playerTeam);

            var volume = game.global.currentMusic.volume;
            game.global.currentMusic.stop();
            game.global.currentMusic = game.global.music[game.worldLeague];
            game.global.currentMusic.play();
            game.global.currentMusic.volume = volume;

            game.state.start('homeMenu');
        }, this);
        playButton.events.onInputOver.add(onOver);
        playButton.events.onInputOut.add(onOut);

        // Button to return to the main menu
        var backButton = game.add.text(game.world.centerX - 80, 440, game.global.optionsLabel.BACK, optionStyle);
        backButton.anchor.setTo(0.5, 0.5);
        backButton.anchor.x = Math.round(backButton.width * 0.5) / backButton.width;
        backButton.inputEnabled = true;
        backButton.input.useHandCursor = true;
        backButton.events.onInputUp.add(function (target) {
            game.state.start('menu');
        });
        backButton.events.onInputOver.add(onOver);
        backButton.events.onInputOut.add(onOut);

    }
};