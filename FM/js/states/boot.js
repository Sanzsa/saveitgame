var bootState = {

    /*******************************************
     *           AUXILIARY FUNCTIONS           *
     *******************************************

    /* *****************************************
     * loadScripts
     *
     * Load the js files needed to run the game.
     * *****************************************/
    loadScripts: function () {
        game.load.script('menu', 'js/states/menu.js');
        game.load.script('options', 'js/states/options.js');
        game.load.script('instructions', 'js/states/instructions.js');
        game.load.script('credits', 'js/states/credits.js');
        game.load.script('play', 'js/states/play.js');
        game.load.script('playOnline', 'js/states/playOnline.js');
        game.load.script('homeMenu', 'js/states/homeMenu.js');
        game.load.script('leagueMenu', 'js/states/leagueMenu.js');
        game.load.script('friendlyMenu', 'js/states/friendlyMenu.js');
        game.load.script('teamCreate', 'js/states/teamCreate.js');
        game.load.script('endLeague', 'js/states/endLeague.js');

        game.load.script('Player','js/classes/Player.js');
        game.load.script('Footballer','js/classes/Footballer.js');
        game.load.script('Goalkeeper','js/classes/Goalkeeper.js');
        game.load.script('League', 'js/classes/League.js');
        game.load.script('Team', 'js/classes/Team.js');
        game.load.script('Ball','js/classes/Ball.js');
        game.load.script('Goal', 'js/classes/Goal.js');
        game.load.script('Action', 'js/classes/Action.js');
        game.load.script('FoxEvaluator', 'js/classes/FoxEvaluator.js');
        game.load.script('DonkeyEvaluator', 'js/classes/DonkeyEvaluator.js');
        game.load.script('Emoji', 'js/classes/Emoji.js');
        game.load.script('AI', 'js/auxiliary/AI.js');
        game.load.script('UI', 'js/auxiliary/UI.js');
        game.load.script('nameGen', 'js/auxiliary/nameGen.js');
        game.load.script('saveLoad', 'js/auxiliary/saveLoad.js');
        game.load.script('gaussianDist', 'js/auxiliary/auxiliary.js');

        game.load.script('Client', 'js/client.js');
    },

    /* *****************************************
     * loadMusic
     *
     * Load music.
     * *****************************************/
    loadMusic: function () {
        game.load.audio('whistle', ['assets/sounds/whistle.mp3']);
        game.load.audio('endGameWhistle', ['assets/sounds/endGameWhistle.mp3']);
        game.load.audio('crowd1', ['assets/sounds/crowd1.mp3']);
        game.load.audio('rewind', ['assets/sounds/rewind.mp3']);
        game.load.audio('error', ['assets/sounds/error.mp3']);

        game.load.audio('menu', ['assets/music/menu.ogg']);
        game.load.audio('azeroth', ['assets/music/azeroth.ogg']);
        game.load.audio('starWars', ['assets/music/starWars.ogg']);
        game.load.audio('nirn', ['assets/music/nirn.ogg']);
        game.load.audio('harryPotter', ['assets/music/harryPotter.ogg']);
    },

    /* *****************************************
     * loadImages
     *
     * Load the images necessary for the sprites
     * and backgrounds used.
     * *****************************************/
    loadImages: function () {
        // Load the players
        // - Cocoa
        game.load.spritesheet('cocoa_green', 'assets/img/cocoa/cocoa_green.png', 21, 39, 32);
        game.load.spritesheet('cocoa_red', 'assets/img/cocoa/cocoa_red.png', 21, 39, 32);
        game.load.spritesheet('cocoa_white', 'assets/img/cocoa/cocoa_white.png', 21, 39, 32);
        game.load.spritesheet('cocoa_yellow', 'assets/img/cocoa/cocoa_yellow.png', 21, 39, 32);

        // - Coffee
        game.load.spritesheet('coffee_green', 'assets/img/coffee/coffee_green.png', 21, 39, 32);
        game.load.spritesheet('coffee_red', 'assets/img/coffee/coffee_red.png', 21, 39, 32);
        game.load.spritesheet('coffee_white', 'assets/img/coffee/coffee_white.png', 21, 39, 32);
        game.load.spritesheet('coffee_yellow', 'assets/img/coffee/coffee_yellow.png', 21, 39, 32);

        // - Milk
        game.load.spritesheet('milk_green', 'assets/img/milk/milk_green.png', 21, 39, 32);
        game.load.spritesheet('milk_red', 'assets/img/milk/milk_red.png', 21, 39, 32);
        game.load.spritesheet('milk_white', 'assets/img/milk/milk_white.png', 21, 39, 32);
        game.load.spritesheet('milk_yellow', 'assets/img/milk/milk_yellow.png', 21, 39, 32);

        // - Vanilla
        game.load.spritesheet('vanilla_green', 'assets/img/vanilla/vanilla_green.png', 21, 39, 32);
        game.load.spritesheet('vanilla_red', 'assets/img/vanilla/vanilla_red.png', 21, 39, 32);
        game.load.spritesheet('vanilla_white', 'assets/img/vanilla/vanilla_white.png', 21, 39, 32);
        game.load.spritesheet('vanilla_yellow', 'assets/img/vanilla/vanilla_yellow.png', 21, 39, 32);

        // Emojis
        game.load.image('emo_greetings', 'assets/img/emojis/greetings.png');
        game.load.image('emo_wellPlayed', 'assets/img/emojis/wellPlayed.png');
        game.load.image('emo_wow', 'assets/img/emojis/wow.png');
        game.load.image('emo_oops', 'assets/img/emojis/oops.png');
        game.load.image('emo_angry', 'assets/img/emojis/angry.png');

        // Load the title
        game.load.image('title', 'assets/img/title1.png');

        // Load the ui assets
        game.load.image('matchBar', 'assets/img/matchBar.png');
        game.load.image('menuBar', 'assets/img/menuBar.png');

        game.load.image('redball', 'assets/img/matchicons/red.png');
        game.load.image('whiteball', 'assets/img/matchicons/white.png');
        game.load.image('yellowball', 'assets/img/matchicons/yellow.png');
        game.load.image('greenball', 'assets/img/matchicons/green.png');

        game.load.image('redball_hover', 'assets/img/matchicons/red_hover.png');
        game.load.image('whiteball_hover', 'assets/img/matchicons/white_hover.png');
        game.load.image('yellowball_hover', 'assets/img/matchicons/yellow_hover.png');
        game.load.image('greenball_hover', 'assets/img/matchicons/green_hover.png');

        game.load.image('marker', 'assets/img/matchicons/marker.png');
        game.load.image('selector', 'assets/img/menubuttons/selector.png');

        game.load.image('arrowkeys', 'assets/img/instructions/arrowkeys.png');
        game.load.image('zKey', 'assets/img/instructions/Z.png');
        game.load.image('xKey', 'assets/img/instructions/X.png');
        game.load.image('cKey', 'assets/img/instructions/C.png');
        game.load.image('arrowkeys_gamepad', 'assets/img/instructions/arrowkeys_gamepad.png');
        game.load.image('zKey_gamepad', 'assets/img/instructions/Z_gamepad.png');
        game.load.image('xKey_gamepad', 'assets/img/instructions/X_gamepad.png');
        game.load.image('cKey_gamepad', 'assets/img/instructions/C_gamepad.png');

        game.load.image('inputBG', 'assets/img/inputBG.png');
        game.load.image('shadowBG', 'assets/img/menuCreateBG.png');
        game.load.image('logShadowBG', 'assets/img/loginInputBG.png');
        game.load.image('erase', 'assets/img/erase.png');
        game.load.image('erase_orange', 'assets/img/erase_orange.png');


        game.load.image('slide_bar', 'assets/img/pointdistribution/bar.png');
        game.load.image('slide_select', 'assets/img/pointdistribution/marker.png');
        game.load.image('plus', 'assets/img/pointdistribution/plus.png');
        game.load.image('minus', 'assets/img/pointdistribution/minus.png');

        game.load.image('lightRow', 'assets/img/table/lightRow.png');
        game.load.image('darkRow', 'assets/img/table/darkRow.png');

        // Load the ball
        game.load.image('ball', 'assets/img/football.png');
        game.load.image('ball_credits', 'assets/img/football_credits.png');

        // Load the field
        game.load.image('field', 'assets/img/field.jpg');

        // Load stadium background
        game.load.image('stadium', 'assets/img/stadium.png');

        // Load walls and goal
        game.load.image('wallV', 'assets/wallVertical.png');
        game.load.image('wallH', 'assets/wallHorizontal.png');
        game.load.image('goalV', 'assets/img/goalV.png');
        game.load.image('goalHLeft', 'assets/img/goalHLeft.png');
        game.load.image('goalHRight', 'assets/img/goalHRight.png');

        // Load a new asset that we will use in the menu state
        game.load.image('azeroth', 'assets/img/azeroth.jpg');
        game.load.image('starWars', 'assets/img/starwars.jpg');
        game.load.image('nirn', 'assets/img/nirn.jpg');
        game.load.image('harryPotter', 'assets/img/harryPotter.jpg');

        // Load menu options
        game.load.image('menuHome', 'assets/img/menubuttons/menuHome.png');
        game.load.image('menuHome_hover', 'assets/img/menubuttons/menuHome_hover.png');
        game.load.image('menuLeague', 'assets/img/menubuttons/menuLeague.png');
        game.load.image('menuLeague_hover', 'assets/img/menubuttons/menuLeague_hover.png');
        game.load.image('menuFriendly', 'assets/img/menubuttons/menuFriendly.png');
        game.load.image('menuFriendly_hover', 'assets/img/menubuttons/menuFriendly_hover.png');
        game.load.image('menuSave', 'assets/img/menubuttons/menuSave.png');
        game.load.image('menuSave_hover', 'assets/img/menubuttons/menuSave_hover.png');
        game.load.image('menuExit', 'assets/img/menubuttons/menuExit.png');
        game.load.image('menuExit_hover', 'assets/img/menubuttons/menuExit_hover.png');
        // Spanish menu options
        game.load.image('menuHome_es', 'assets/img/menubuttons/menuHome_es.png');
        game.load.image('menuHome_es_hover', 'assets/img/menubuttons/menuHome_es_hover.png');
        game.load.image('menuLeague_es', 'assets/img/menubuttons/menuLeague_es.png');
        game.load.image('menuLeague_es_hover', 'assets/img/menubuttons/menuLeague_es_hover.png');
        game.load.image('menuFriendly_es', 'assets/img/menubuttons/menuFriendly_es.png');
        game.load.image('menuFriendly_es_hover', 'assets/img/menubuttons/menuFriendly_es_hover.png');
        game.load.image('menuSave_es', 'assets/img/menubuttons/menuSave_es.png');
        game.load.image('menuSave_es_hover', 'assets/img/menubuttons/menuSave_es_hover.png');
        game.load.image('menuExit_es', 'assets/img/menubuttons/menuExit_es.png');
        game.load.image('menuExit_es_hover', 'assets/img/menubuttons/menuExit_es_hover.png');

        // Load formation images
        game.load.image('1-3-2', 'assets/img/formations/1-3-2.png');
        game.load.image('1-4-1', 'assets/img/formations/1-4-1.png');
        game.load.image('2-1-3', 'assets/img/formations/2-1-3.png');
        game.load.image('2-2-2', 'assets/img/formations/2-2-2.png');
        game.load.image('2-3-1', 'assets/img/formations/2-3-1.png');
        game.load.image('3-1-2', 'assets/img/formations/3-1-2.png');
        game.load.image('3-2-1', 'assets/img/formations/3-2-1.png');
        game.load.image('3-3', 'assets/img/formations/3-3.png');
        game.load.image('4-1-1', 'assets/img/formations/4-1-1.png');

        // Load play button
        game.load.image('play', 'assets/img/menubuttons/play.png');
        game.load.image('pause', 'assets/img/menubuttons/pause.png');

        // Load AI animal icons
        game.load.image('fox', 'assets/img/ai/fox.png');
        game.load.image('donkey', 'assets/img/ai/donkey.png');
        game.load.image('fox_hover', 'assets/img/ai/fox_hover.png');
        game.load.image('donkey_hover', 'assets/img/ai/donkey_hover.png');

        // Load Control icons
        game.load.image('controller', 'assets/img/control/controller.png');
        game.load.image('simulate', 'assets/img/control/simulate.png');
        game.load.image('controller_hover', 'assets/img/control/controller_hover.png');
        game.load.image('simulate_hover', 'assets/img/control/simulate_hover.png');
    },

    /* *****************************************
     * addStates
     *
     * Add game states.
     * *****************************************/
    addStates: function () {
        // Add all the remaining states
        game.state.add('menu', menuState);
        game.state.add('options', optionsState);
        game.state.add('instructions', instructionsState);
        game.state.add('credits', creditsState);
        game.state.add('play', playState);
        game.state.add('playOnline', playOnlineState);
        game.state.add('homeMenu', homeMenuState);
        game.state.add('leagueMenu', leagueMenuState);
        game.state.add('friendlyMenu', friendlyMenuState);
        game.state.add('teamCreate', teamCreateState);
        game.state.add('endLeague', endLeagueState);
    },


    /* *****************************************
     * addMusic
     *
     * Add music and sounds to the game.
     * *****************************************/
    addMusic: function () {
        // Sounds
        game.global.ringSound = game.add.audio('whistle');
        game.global.endGameSound = game.add.audio('endGameWhistle');
        game.global.crowdSound = game.add.audio('crowd1');
        game.global.saveSound = game.add.audio('rewind');
        game.global.failLoadSound = game.add.audio('error');

        // Music
        game.global.music = {
            'menu' : game.add.audio('menu'),
            'azeroth' : game.add.audio('azeroth'),
            'starWars' : game.add.audio('starWars'),
            'nirn' : game.add.audio('nirn'),
            'harryPotter' : game.add.audio('harryPotter')
        };
        game.global.music['menu'].loop = true;
        game.global.music['azeroth'].loop = true;
        game.global.music['starWars'].loop = true;
        game.global.music['nirn'].loop = true;
        game.global.music['harryPotter'].loop = true;

        // Start menu music
        game.global.currentMusic = game.global.music['menu'];
        game.global.currentMusic.play();
    },

    /* *****************************************
     * setGamepad
     *
     * Check if there is a gamepad connected and
     * set listeners for gamepad support.
     * *****************************************/
    setGamepad: function () {
        game.gamepad = navigator.getGamepads()[0];

        if(game.gamepad)
            game.global.gamepadConnected = true;
        else
            game.global.gamepadConnected = false;

        var onGamepadConnect = function(event) {
            game.gamepad = navigator.getGamepads()[0];
            game.global.gamepadConnected = true;
        };

        var onGamepadDisconnect = function (event) {
            game.gamepad = null;
            game.global.gamepadConnected= false;
        };

        window.addEventListener('gamepadconnected', onGamepadConnect, false);
        window.addEventListener('gamepaddisconnected', onGamepadDisconnect, false);
    },


    /*******************************************
     *            PHASER FUNCTIONS             *
     *******************************************/

    /* *****************************************
     * init
     *
     * Set loading label and progress bar.
     * *****************************************/
    init: function () {
        // Add a background image
        var bgimage = game.add.image(0, 0, 'background');
        bgimage.scale.setTo(1.75);
        // Add a background image
        var bgshadow = game.add.image(0, 0, 'bg_shadow');
        bgshadow.scale.setTo(2.6);

        // Add a 'loading...' label on the screen
        var loadingLabel = game.add.text(game.world.centerX, 150, 'Loading...',{ font: '30px PopWarner',
            fill: game.global.colors.WHITE });
        loadingLabel.anchor.setTo(0.5, 0.5);
        loadingLabel.anchor.x = Math.round(loadingLabel.width * 0.5) / loadingLabel.width;

        // Display the progress bar
        this.progressBar = game.add.sprite(game.world.centerX, 200, 'progressBar');
        this.progressBar.anchor.setTo(0.5, 0.5);
    },

    /* *****************************************
     * preload
     *
     * Set background color, physics, and call
     * auxiliary functions.
     * *****************************************/
    preload: function () {
        game.stage.disableVisibilityChange = true;
        game.load.setPreloadSprite(this.progressBar);

        // Set some game settings
        game.stage.backgroundColor = game.global.colors.BGBLUE;
        game.physics.startSystem(Phaser.Physics.P2JS);
        game.physics.p2.setImpactEvents(true);
        game.physics.p2.restitution = 0.8;

        this.loadScripts();
        this.loadMusic();
        this.loadImages();
    },

    /* *****************************************
     * create
     *
     * Add states, music and start menu state.
     * *****************************************/
    create: function () {
        this.addStates();
        this.addMusic();

        this.setGamepad();

        game.firstMenu = true;

        // Start the load state
        game.state.start('menu');
    }
};