
var homeMenuState = {

    // UI.js option specs
    menuOptionWidth : 400,
    menuOptionScale : 0.5,

    /*******************************************
     *            PHASER FUNCTIONS             *
     *******************************************/

    /* *****************************************
     * preload
     *
     * Set optionCount to 1.
     * *****************************************/
    preload: function () {
        this.optionCount = 1;

        // Set league title specifications
        switch (game.worldLeague) {
            case 'azeroth':
                this.leagueTxt = "Azeroth Premier League";
                this.leagueStyle = '24pt LifeCraft';
                break;
            case 'starWars':
                this.leagueTxt = "Star Soccer";
                this.leagueStyle = '22pt StarJedi';
                break;
            case 'nirn':
                this.leagueTxt = "The Elder Soccer";
                this.leagueStyle = '26pt Planewalker';
                break;
            case 'harryPotter':
                this.leagueTxt = "Wizard League";
                this.leagueStyle = '30pt HarryP';
                break;
        }
    },

    /* *****************************************
     * create
     *
     * Create the menu options and add the
     * callback functions.
     * *****************************************/
    create: function() {
        // Add a background image
        game.add.image(0, 55, game.worldLeague);

        var shadow = game.add.image(510, game.world.centerY + 25, 'shadowBG');
        shadow.anchor.setTo(0.5, 0.5);
        shadow.scale.setTo(0.9, 1.4);

        // Create header using UI.js
        createHeader();

        // Create lateral menu using UI.js
        createLateralMenu(game, this);

        // Team table titles
        var leagueTitle = game.add.text(300, 100, this.leagueTxt,
            {font: this.leagueStyle, fill: game.global.colors.GOLDEN,
                stroke: game.global.colors.DARKGRAY, strokeThickness: 3});
        var gamesPlayedTitle = game.add.text(554, 150, "Pl",
            {font: '12pt PopWarner', fill: game.global.colors.ORANGE,
                stroke: game.global.colors.DARKGRAY, strokeThickness: 1});
        gamesPlayedTitle.anchor.setTo(0.5);
        gamesPlayedTitle.anchor.x = Math.round(gamesPlayedTitle.width * 0.5) / gamesPlayedTitle.width;
        var gamesWonTitle = game.add.text(583, 150, "W",
            {font: '12pt PopWarner', fill: game.global.colors.ORANGE,
                stroke: game.global.colors.DARKGRAY, strokeThickness: 1});
        gamesWonTitle.anchor.setTo(0.5);
        gamesWonTitle.anchor.x = Math.round(gamesWonTitle.width * 0.5) / gamesWonTitle.width;
        var gamesDrawnTitle = game.add.text(612, 150, "D",
            {font: '12pt PopWarner', fill: game.global.colors.ORANGE,
                stroke: game.global.colors.DARKGRAY, strokeThickness: 1});
        gamesDrawnTitle.anchor.setTo(0.5);
        gamesDrawnTitle.anchor.x = Math.round(gamesDrawnTitle.width * 0.5) / gamesDrawnTitle.width;
        var gamesLostTitle = game.add.text(641, 150, "L",
            {font: '12pt PopWarner', fill: game.global.colors.ORANGE,
                stroke: game.global.colors.DARKGRAY, strokeThickness: 1});
        gamesLostTitle.anchor.setTo(0.5);
        gamesLostTitle.anchor.x = Math.round(gamesLostTitle.width * 0.5) / gamesLostTitle.width;
        var goalsScoredTitle = game.add.text(670, 150, "G.F.",
            {font: '12pt PopWarner', fill: game.global.colors.ORANGE,
                stroke: game.global.colors.DARKGRAY, strokeThickness: 1});
        goalsScoredTitle.anchor.setTo(0.5);
        goalsScoredTitle.anchor.x = Math.round(goalsScoredTitle.width * 0.5) / goalsScoredTitle.width;
        var goalsAgainstTitle = game.add.text(699, 150, "G.A.",
            {font: '12pt PopWarner', fill: game.global.colors.ORANGE,
                stroke: game.global.colors.DARKGRAY, strokeThickness: 1});
        goalsAgainstTitle.anchor.setTo(0.5);
        goalsAgainstTitle.anchor.x = Math.round(goalsAgainstTitle.width * 0.5) / goalsAgainstTitle.width;
        var pointsTitle = game.add.text(728, 150, 'Pts.',
            {font: '12pt PopWarner', fill: game.global.colors.ORANGE,
                stroke: game.global.colors.DARKGRAY, strokeThickness: 1});
        pointsTitle.anchor.setTo(0.5);
        pointsTitle.anchor.x = Math.round(pointsTitle.width * 0.5) / pointsTitle.width;

        // Sort teams depending on their score
        var auxTeams = game.league.teams.slice();
        auxTeams.sort(compare);

        // Render each team
        for (var i = 0; i < game.league.teams.length; i++) {
            var currentRow;

            // Alternate color for a neat table
            if (i%2) {
                currentRow = "darkRow";
            } else {
                currentRow = "lightRow";
            }
            var bg = game.add.image(300, (i * 25) + 157,currentRow);
            var pos = game.add.text(280, (i * 25) + 159, i + 1,
                {font: '16pt PopWarner', fill: game.global.colors.ORANGE,
                    stroke: game.global.colors.DARKGRAY, strokeThickness: 2});
            var name = game.add.text(310, (i * 25) + 161, auxTeams[i].name,
                {font: '12pt PopWarner', fill: game.global.colors.DARKGRAY});
            var gamesPlayed = game.add.text(554, (i * 25) + 173, game.league.matchIndex,
                {font: '12pt PopWarner', fill: game.global.colors.DARKGRAY});
            gamesPlayed.anchor.setTo(0.5);
            gamesPlayed.anchor.x = Math.round(gamesPlayed.width * 0.5) / gamesPlayed.width;
            var gamesWon = game.add.text(583, (i * 25) + 173, auxTeams[i].gamesWon,
                {font: '12pt PopWarner', fill: game.global.colors.DARKGRAY});
            gamesWon.anchor.setTo(0.5);
            gamesWon.anchor.x = Math.round(gamesWon.width * 0.5) / gamesWon.width;
            var gamesDrawn = game.add.text(612, (i * 25) + 173, auxTeams[i].gamesDrawn,
                {font: '12pt PopWarner', fill: game.global.colors.DARKGRAY});
            gamesDrawn.anchor.setTo(0.5);
            gamesDrawn.anchor.x = Math.round(gamesDrawn.width * 0.5) / gamesDrawn.width;
            var gamesLost = game.add.text(641, (i * 25) + 173, auxTeams[i].gamesLost,
                {font: '12pt PopWarner', fill: game.global.colors.DARKGRAY});
            gamesLost.anchor.setTo(0.5);
            gamesLost.anchor.x = Math.round(gamesLost.width * 0.5) / gamesLost.width;
            var goalsScored = game.add.text(670, (i * 25) + 173, auxTeams[i].goalsScored,
                {font: '12pt PopWarner', fill: game.global.colors.DARKGRAY});
            goalsScored.anchor.setTo(0.5);
            goalsScored.anchor.x = Math.round(goalsScored.width * 0.5) / goalsScored.width;
            var goalsAgainst = game.add.text(699, (i * 25) + 173, auxTeams[i].goalsAgainst,
                {font: '12pt PopWarner', fill: game.global.colors.DARKGRAY});
            goalsAgainst.anchor.setTo(0.5);
            goalsAgainst.anchor.x = Math.round(goalsAgainst.width * 0.5) / goalsAgainst.width;
            var points = game.add.text(728, (i * 25) + 173, auxTeams[i].points,
                {font: '12pt PopWarner', fill: game.global.colors.DARKGRAY});
            points.anchor.setTo(0.5);
            points.anchor.x = Math.round(points.width * 0.5) / points.width;
        }

        // Write the legend
        var gamesPlayedLegend = game.add.text(458, 475, "Pl - " + game.global.tableLegend.PLAYED,
            {font: '10pt Arial', fill: "white"});
        gamesPlayedLegend.anchor.setTo(0,0.5);
        var gamesWonLegend = game.add.text(458, 490, "W - " + game.global.tableLegend.WON,
            {font: '10pt Arial', fill: "white"});
        gamesWonLegend.anchor.setTo(0,0.5);
        var gamesDrawnLegend = game.add.text(458, 505, "D - " + game.global.tableLegend.DRAWN,
            {font: '10pt Arial', fill: "white"});
        gamesDrawnLegend.anchor.setTo(0,0.5);
        var gamesLostLegend = game.add.text(458, 520, "L - " + game.global.tableLegend.LOST,
            {font: '10pt Arial', fill: "white"});
        gamesLostLegend.anchor.setTo(0,0.5);
        var goalsScoredLegend = game.add.text(605, 475, "G.F. - " + game.global.tableLegend.FOR,
            {font: '10pt Arial', fill: "white"});
        goalsScoredLegend.anchor.setTo(0,0.5);
        var goalsAgainstLegend = game.add.text(605, 490, "G.A. - " + game.global.tableLegend.AGAINST,
            {font: '10pt Arial', fill: "white"});
        goalsAgainstLegend.anchor.setTo(0,0.5);
        var pointsLegend = game.add.text(605, 505, "Pts. - " + game.global.tableLegend.POINTS,
            {font: '10pt Arial', fill: "white"});
        pointsLegend.anchor.setTo(0,0.5);

        // Name label
        var text = game.add.text(game.world.centerX - 140, 200, game.global.selectorLabel.HOME,
            {font: '32pt PopWarner', fill: game.global.colors.ORANGE,
                stroke: game.global.colors.DARKGRAY, strokeThickness: 4});
        text.anchor.setTo(0.5,0.5);
        text.anchor.x = Math.round(text.width * 0.5) / text.width;
    }
};