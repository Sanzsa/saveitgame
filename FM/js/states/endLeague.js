// We create our play state
var endLeagueState = {

    /*******************************************
     *           AUXILIARY FUNCTIONS           *
     *******************************************/

    /* *****************************************
     * addLeagueOption
     *
     * Factory for the easy creation of menu
     * options.
     * *****************************************/
    addLeagueOption: function (text, id, font) {
        var optionStyle = null;

        // Font size adjustment
        if (font == 'LifeCraft') {
            optionStyle = {font: '20pt ' + font, fill: game.global.colors.LIGHTBLUE, align: 'left'};
        } else if (font == 'Planewalker') {
            optionStyle = {font: '20pt ' + font, fill: game.global.colors.LIGHTBLUE, align: 'left'};
        } else if (font == 'HarryP') {
            optionStyle = {font: '24pt ' + font, fill: game.global.colors.LIGHTBLUE, align: 'left'};
        } else {
            optionStyle = {font: '15pt ' + font, fill: game.global.colors.LIGHTBLUE, align: 'left'};
        }
        txt = game.add.text(game.world.centerX + 145, (this.optionCount * 35) + 310, text, optionStyle);
        txt.anchor.setTo(0, 0.5);
        txt.anchor.y = Math.round(txt.height * 0.5) / txt.height;

        var onOver = function (target) {
            target.fill = game.global.colors.DARKBLUE;
        };
        var onOut = function (target) {
            target.fill = game.global.colors.LIGHTBLUE;
        };
        txt.inputEnabled = true;
        txt.input.useHandCursor = true;
        txt.events.onInputUp.add(function (target) {
            game.worldLeague = id;
            this.leagueMarker.y = target.y;
            this.leagueMarker.style = target.style;
            switch (font) {
                // Horde banner
                case 'LifeCraft':
                    this.leagueMarker.text = '[';
                    break;
                // Rebel symbol
                case 'StarJedi':
                    this.leagueMarker.text = '$';
                    break;
                // Cross
                case 'Planewalker':
                    this.leagueMarker.text = '+';
                    break;
                // Lightning
                case 'HarryP':
                    this.leagueMarker.text = '\\';
                    break;
            }
        }, this);
        txt.events.onInputOver.add(onOver);
        txt.events.onInputOut.add(onOut);
        this.optionCount++;
    },


    /*******************************************
     *            PHASER FUNCTIONS             *
     *******************************************/

    /* *****************************************
     * preload
     *
     * Set optionCount and font
     * *****************************************/
    preload: function () {
        this.optionCount = 1;

        // Set league title specifications
        switch (game.worldLeague) {
            case 'azeroth':
                this.leagueTxt = "Azeroth Premier League";
                this.leagueStyle = '24pt LifeCraft';
                break;
            case 'starWars':
                this.leagueTxt = "Star Soccer";
                this.leagueStyle = '22pt StarJedi';
                place = starWars;
                break;
            case 'nirn':
                this.leagueTxt = "The Elder Soccer";
                this.leagueStyle = '26pt Planewalker';
                place = nirn;
                break;
            case 'harryPotter':
                this.leagueTxt = "Wizard League";
                this.leagueStyle = '30pt HarryP';
                place = harryPotter;
                break;
        }
    },

    /* *****************************************
     * create
     *
     * Create the necessary elements for the
     * game.
     * *****************************************/
    create: function () {
        // Add a background image
        var bgimage = game.add.image(0, 55, game.worldLeague);

        // Set default league
        game.worldLeague = 'azeroth';

        // Create header using UI.js
        createHeader();

        // Set back button
        var back = game.add.image(game.world.centerX, game.world.centerY + 25, 'shadowBG');
        back.anchor.setTo(0.5, 0.5);
        back.scale.setTo(1.35,1.4);
        var onOver = function (target) {
            target.fill = game.global.colors.ORANGE;
            target.stroke = game.global.colors.DARKGRAY;
        };
        var onOut = function (target) {
            target.fill = game.global.colors.DARKGRAY;
            target.stroke = game.global.colors.ORANGE;
        };

        // Setup for the team table
        var optionStyle = {
            font: '22pt PopWarner', fill: game.global.colors.DARKGRAY, align: 'left',
            stroke: game.global.colors.ORANGE, strokeThickness: 3
        };

        // Team table titles
        var leagueTitle = game.add.text(50, 100, this.leagueTxt,
            {font: this.leagueStyle, fill: game.global.colors.GOLDEN,
                stroke: game.global.colors.DARKGRAY, strokeThickness: 3});
        var gamesPlayedTitle = game.add.text(304, 150, "Pl",
            {font: '12pt PopWarner', fill: game.global.colors.ORANGE,
                stroke: game.global.colors.DARKGRAY, strokeThickness: 1});
        gamesPlayedTitle.anchor.setTo(0.5);
        gamesPlayedTitle.anchor.x = Math.round(gamesPlayedTitle.width * 0.5) / gamesPlayedTitle.width;
        var gamesWonTitle = game.add.text(333, 150, "W",
            {font: '12pt PopWarner', fill: game.global.colors.ORANGE,
                stroke: game.global.colors.DARKGRAY, strokeThickness: 1});
        gamesWonTitle.anchor.setTo(0.5);
        gamesWonTitle.anchor.x = Math.round(gamesWonTitle.width * 0.5) / gamesWonTitle.width;
        var gamesDrawnTitle = game.add.text(362, 150, "D",
            {font: '12pt PopWarner', fill: game.global.colors.ORANGE,
                stroke: game.global.colors.DARKGRAY, strokeThickness: 1});
        gamesDrawnTitle.anchor.setTo(0.5);
        gamesDrawnTitle.anchor.x = Math.round(gamesDrawnTitle.width * 0.5) / gamesDrawnTitle.width;
        var gamesLostTitle = game.add.text(391, 150, "L",
            {font: '12pt PopWarner', fill: game.global.colors.ORANGE,
                stroke: game.global.colors.DARKGRAY, strokeThickness: 1});
        gamesLostTitle.anchor.setTo(0.5);
        gamesLostTitle.anchor.x = Math.round(gamesLostTitle.width * 0.5) / gamesLostTitle.width;
        var goalsScoredTitle = game.add.text(420, 150, "G.F.",
            {font: '12pt PopWarner', fill: game.global.colors.ORANGE,
                stroke: game.global.colors.DARKGRAY, strokeThickness: 1});
        goalsScoredTitle.anchor.setTo(0.5);
        goalsScoredTitle.anchor.x = Math.round(goalsScoredTitle.width * 0.5) / goalsScoredTitle.width;
        var goalsAgainstTitle = game.add.text(449, 150, "G.A.",
            {font: '12pt PopWarner', fill: game.global.colors.ORANGE,
                stroke: game.global.colors.DARKGRAY, strokeThickness: 1});
        goalsAgainstTitle.anchor.setTo(0.5);
        goalsAgainstTitle.anchor.x = Math.round(goalsAgainstTitle.width * 0.5) / goalsAgainstTitle.width;
        var pointsTitle = game.add.text(478, 150, 'Pts.',
            {font: '12pt PopWarner', fill: game.global.colors.ORANGE,
                stroke: game.global.colors.DARKGRAY, strokeThickness: 1});
        pointsTitle.anchor.setTo(0.5);
        pointsTitle.anchor.x = Math.round(pointsTitle.width * 0.5) / pointsTitle.width;

        // Sort teams depending on their score
        var auxTeams = game.league.teams.slice();
        auxTeams.sort(compare);

        var playerPos;

        // Render each team
        for (var i = 0; i < game.league.teams.length; i++) {
            var currentRow;

            // Alternate color for a neat table
            if (i%2) {
                currentRow = "darkRow";
            } else {
                currentRow = "lightRow";
            }
            if (auxTeams[i] == game.playerTeam) {
                playerPos = i + 1;
            }
            var bg = game.add.image(50, (i * 25) + 157,currentRow);
            var pos = game.add.text(30, (i * 25) + 159, i + 1,
                {font: '16pt PopWarner', fill: game.global.colors.ORANGE,
                    stroke: game.global.colors.DARKGRAY, strokeThickness: 2});
            var name = game.add.text(60, (i * 25) + 161, auxTeams[i].name,
                {font: '12pt PopWarner', fill: game.global.colors.DARKGRAY});
            var gamesPlayed = game.add.text(304, (i * 25) + 173, game.league.matchIndex,
                {font: '12pt PopWarner', fill: game.global.colors.DARKGRAY});
            gamesPlayed.anchor.setTo(0.5);
            gamesPlayed.anchor.x = Math.round(gamesPlayed.width * 0.5) / gamesPlayed.width;
            var gamesWon = game.add.text(333, (i * 25) + 173, auxTeams[i].gamesWon,
                {font: '12pt PopWarner', fill: game.global.colors.DARKGRAY});
            gamesWon.anchor.setTo(0.5);
            gamesWon.anchor.x = Math.round(gamesWon.width * 0.5) / gamesWon.width;
            var gamesDrawn = game.add.text(362, (i * 25) + 173, auxTeams[i].gamesDrawn,
                {font: '12pt PopWarner', fill: game.global.colors.DARKGRAY});
            gamesDrawn.anchor.setTo(0.5);
            gamesDrawn.anchor.x = Math.round(gamesDrawn.width * 0.5) / gamesDrawn.width;
            var gamesLost = game.add.text(391, (i * 25) + 173, auxTeams[i].gamesLost,
                {font: '12pt PopWarner', fill: game.global.colors.DARKGRAY});
            gamesLost.anchor.setTo(0.5);
            gamesLost.anchor.x = Math.round(gamesLost.width * 0.5) / gamesLost.width;
            var goalsScored = game.add.text(420, (i * 25) + 173, auxTeams[i].goalsScored,
                {font: '12pt PopWarner', fill: game.global.colors.DARKGRAY});
            goalsScored.anchor.setTo(0.5);
            goalsScored.anchor.x = Math.round(goalsScored.width * 0.5) / goalsScored.width;
            var goalsAgainst = game.add.text(449, (i * 25) + 173, auxTeams[i].goalsAgainst,
                {font: '12pt PopWarner', fill: game.global.colors.DARKGRAY});
            goalsAgainst.anchor.setTo(0.5);
            goalsAgainst.anchor.x = Math.round(goalsAgainst.width * 0.5) / goalsAgainst.width;
            var points = game.add.text(478, (i * 25) + 173, auxTeams[i].points,
                {font: '12pt PopWarner', fill: game.global.colors.DARKGRAY});
            points.anchor.setTo(0.5);
            points.anchor.x = Math.round(points.width * 0.5) / points.width;
        }

        // Write the legend
        var gamesPlayedLegend = game.add.text(208, 475, "Pl - " + game.global.tableLegend.PLAYED,
            {font: '10pt Arial', fill: "white"});
        gamesPlayedLegend.anchor.setTo(0,0.5);
        var gamesWonLegend = game.add.text(208, 490, "W - " + game.global.tableLegend.WON,
            {font: '10pt Arial', fill: "white"});
        gamesWonLegend.anchor.setTo(0,0.5);
        var gamesDrawnLegend = game.add.text(208, 505, "D - " + game.global.tableLegend.DRAWN,
            {font: '10pt Arial', fill: "white"});
        gamesDrawnLegend.anchor.setTo(0,0.5);
        var gamesLostLegend = game.add.text(208, 520, "L - " + game.global.tableLegend.LOST,
            {font: '10pt Arial', fill: "white"});
        gamesLostLegend.anchor.setTo(0,0.5);
        var goalsScoredLegend = game.add.text(355, 475, "G.F. - " + game.global.tableLegend.FOR,
            {font: '10pt Arial', fill: "white"});
        goalsScoredLegend.anchor.setTo(0,0.5);
        var goalsAgainstLegend = game.add.text(355, 490, "G.A. - " + game.global.tableLegend.AGAINST,
            {font: '10pt Arial', fill: "white"});
        goalsAgainstLegend.anchor.setTo(0,0.5);
        var pointsLegend = game.add.text(355, 505, "Pts. - " + game.global.tableLegend.POINTS,
            {font: '10pt Arial', fill: "white"});
        pointsLegend.anchor.setTo(0,0.5);

        // Text where team name will be written
        this.nameInput = game.add.text(game.world.centerX + 250, 153, game.teamName,
            {font: '24pt PopWarner', fill: game.global.colors.LIGHTBLUE,
                stroke: game.global.colors.DARKGRAY, strokeThickness: 3});
        this.nameInput.anchor.setTo(0.5, 0.5);
        this.nameInput.anchor.x = Math.round(this.nameInput.width * 0.5) / this.nameInput.width;
        this.nameInput.anchor.y = Math.round(this.nameInput.height * 0.5) / this.nameInput.height;

        // Prompt based on league result
        if (playerPos < 4) {
            this.leaguePrompt = game.add.text(game.world.centerX + 250, 200, game.global.endLeagueText.GOODEND,
                {   font: '21pt PopWarner', fill: game.global.colors.ORANGE,
                    stroke: game.global.colors.DARKGRAY, strokeThickness: 1});
        } else if (playerPos > game.global.numTeamLeague - 3) {
            this.leaguePrompt = game.add.text(game.world.centerX + 250, 200, game.global.endLeagueText.BADEND,
                {   font: '21pt PopWarner', fill: game.global.colors.ORANGE,
                    stroke: game.global.colors.DARKGRAY, strokeThickness: 1});
        } else {
            this.leaguePrompt = game.add.text(game.world.centerX + 250, 200, game.global.endLeagueText.NEUTRALEND,
                {   font: '21pt PopWarner', fill: game.global.colors.ORANGE,
                    stroke: game.global.colors.DARKGRAY, strokeThickness: 1});
        }
        this.leaguePrompt.anchor.setTo(0.5, 0.5);
        this.leaguePrompt.anchor.x = Math.round(this.leaguePrompt.width * 0.5) / this.leaguePrompt.width;
        this.leaguePrompt.anchor.y = Math.round(this.leaguePrompt.height * 0.5) / this.leaguePrompt.height;

        // Prompt with your position
        this.leaguePos = game.add.text(game.world.centerX + 250, 240, game.global.endLeagueText.FINISH + playerPos + 'º.',
            {font: '21pt PopWarner', fill: game.global.colors.ORANGE,
                stroke: game.global.colors.DARKGRAY, strokeThickness: 1});
        this.leaguePos.anchor.setTo(0.5, 0.5);
        this.leaguePos.anchor.x = Math.round(this.leaguePos.width * 0.5) / this.leaguePos.width;
        this.leaguePos.anchor.y = Math.round(this.leaguePos.height * 0.5) / this.leaguePos.height;

        // Prompt to choose new league
        this.leagueChoose = game.add.text(game.world.centerX + 250, 290, game.global.endLeagueText.NEWLEAGUE,
            {font: '12pt PopWarner', fill: game.global.colors.ORANGE,
                stroke: game.global.colors.DARKGRAY, strokeThickness: 1});
        this.leagueChoose.anchor.setTo(0.5, 0.5);
        this.leagueChoose.anchor.x = Math.round(this.leagueChoose.width * 0.5) / this.leagueChoose.width;
        this.leagueChoose.anchor.y = Math.round(this.leagueChoose.height * 0.5) / this.leagueChoose.height;

        this.leagueMarker = game.add.text(game.world.centerX + 130, 345, '[',
            {font: '20pt LifeCraft', fill: game.global.colors.DARKBLUE, align: 'left'});
        this.leagueMarker.anchor.setTo(0.5);
        this.leagueMarker.anchor.x = Math.round(this.leagueMarker.width * 0.5) / this.leagueMarker.width;
        this.leagueMarker.anchor.y = Math.round(this.leagueMarker.height * 0.5) / this.leagueMarker.height;
        this.addLeagueOption("Azeroth Premier League", "azeroth", "LifeCraft");
        this.addLeagueOption("The Elder Soccer", "nirn", "Planewalker");
        this.addLeagueOption("Star Soccer", "starWars", "StarJedi");
        this.addLeagueOption("Wizard League", "harryPotter", "HarryP");

        // Button to start the game
        var playButton = game.add.text(game.world.centerX + 250, 500, game.global.optionsLabel.OK, optionStyle);
        playButton.anchor.setTo(0.5, 0.5);
        playButton.anchor.x = Math.round(playButton.width * 0.5) / playButton.width;
        playButton.inputEnabled = true;
        playButton.input.useHandCursor = true;
        playButton.events.onInputUp.add(function (target) {
            game.global.selectedFormation = formationCount;
            game.teamName = this.nameInput.text;
            game.league = new League();
            game.league.buildLeague();
            game.playerTeam = new Team(null, null, game.teamColor, game.teamName, game.global.selectedFormation,
                null, null);

            game.league.teams.splice(0, 0, game.playerTeam);

            var volume = game.global.currentMusic.volume;
            game.global.currentMusic.stop();
            game.global.currentMusic = game.global.music[game.worldLeague];
            game.global.currentMusic.play();
            game.global.currentMusic.volume = volume;

            game.state.start('homeMenu');
        }, this);
        playButton.events.onInputOver.add(onOver);
        playButton.events.onInputOut.add(onOut);
    }
};