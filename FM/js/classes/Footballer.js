// Constants
var playerKick = 300;
var playerPass = 200;

// Class that represents the players
Footballer = function (game, x, y, image, isPlayer, team, evaluator, ball, attributes) {

    // Inherit from Player
    Player.call(this, game, x, y, image, team, evaluator, ball, attributes);

    // Set whether this is controlled by the player
    this.isPlayer = isPlayer;

    /*******************************************
     *           CALLABLE FUNCTIONS            *
     *******************************************/

    /* *****************************************
     * move
     *
     * Moves the player, using AI or input.
     * *****************************************/
    this.move = function () {
        if (this.isPlayer) {
            this.playerMove();
        } else {
            this.aiMove();
        }

        // TODO
        if (this.controlledBall != null) {
            this.moveBall();
        }
    };

    /*******************************************
     *           AUXILIARY FUNCTIONS           *
     *******************************************/

    /* *****************************************
     * playerMove
     *
     * Moves the player, using input.
     * *****************************************/
    this.playerMove = function () {
        // If the left arrow key is pressed
        if (game.cursor.left.isDown ||
            (game.global.gamepadConnected && game.gamepad.axes[0] < -0.5)) {
            if (game.cursor.up.isDown ||
                (game.global.gamepadConnected && game.gamepad.axes[1] < -0.5)) {
                // Move the player to the up left
                this.moveTo(-playerVelocityDiag * this.celerity, -playerVelocityDiag * this.celerity, 'walkuplf', -1, -1);
            } else if (game.cursor.down.isDown ||
                (game.global.gamepadConnected && game.gamepad.axes[1] > 0.5)) {
                // Move the player to the down left
                this.moveTo(-playerVelocityDiag * this.celerity, playerVelocityDiag * this.celerity, 'walkdwlf', -1, 1);
            } else {
                // Move the player to the left
                this.moveTo(-playerVelocity * this.celerity, 0, 'walkleft', -1, 0);
            }
        }
        // If the right arrow key is pressed
        else if (game.cursor.right.isDown ||
            (game.global.gamepadConnected && game.gamepad.axes[0] > 0.5)) {
            if (game.cursor.up.isDown ||
                (game.global.gamepadConnected && game.gamepad.axes[1] < -0.5)) {
                // Move the player to the up right
                this.moveTo(playerVelocityDiag * this.celerity, -playerVelocityDiag * this.celerity, 'walkupri', 1, -1);
            } else if (game.cursor.down.isDown ||
                (game.global.gamepadConnected && game.gamepad.axes[1] > 0.5)) {
                // Move the player to the down right
                this.moveTo(playerVelocityDiag * this.celerity, playerVelocityDiag * this.celerity, 'walkdwri', 1, 1);
            } else {
                // Move the player to the right
                this.moveTo(playerVelocity * this.celerity, 0, 'walkright', 1, 0);
            }
        }
        // If the up arrow key is pressed
        else if (game.cursor.up.isDown ||
            (game.global.gamepadConnected && game.gamepad.axes[1] < -0.5)) {
            // Move the player upwards
            this.moveTo(0, -playerVelocity * this.celerity, 'walkup', 0, -1);
        }
        // If the down arrow key is pressed
        else if (game.cursor.down.isDown ||
            (game.global.gamepadConnected && game.gamepad.axes[1] > 0.5)) {
            // Move the player downwards
            this.moveTo(0, playerVelocity * this.celerity, 'walkdown', 0, 1);
        }
        // If neither the up or down arrow key is pressed
        else {
            // Stop the player
            this.moveTo(0, 0, null, null, null);
        }
    };

    /* *****************************************
     * aiMove
     *
     * Moves the player, using AI.
     * *****************************************/
    this.aiMove = function () {
        this.doAction(this.evaluator.evaluate(this));
    };
};

// Set inheritance and constructor
Footballer.prototype = Object.create(Player.prototype);
Footballer.prototype.constructor = Footballer;