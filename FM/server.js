//TODO: add package.json
var SOCKET = 0;
var DATA = 1;
var BALL = 2;

var express = require('express');
var p2 = require('p2');
var app = express();
var server = require('http').Server(app);
var io = require('socket.io').listen(server);


var TeamMirror = require(__dirname + '/srv/TeamMirror');
var PlayerMirror = require(__dirname + '/srv/PlayerMirror');
var BallMirror = require(__dirname + '/srv/BallMirror');
app.use('/js', express.static(__dirname + '/js'));
app.use('/assets', express.static(__dirname + '/assets'));

app.get('/',function(req,res){
    res.sendFile(__dirname+'/index.html');
});

server.lastPlayerID = 0;
server.players = [];
server.matches = [];

var teams = {}, balls = [];

server.listen(process.env.PORT || 8081,function(){
    console.log('Listening on '+server.address().port);
});

io.on('connection',function(socket){

    socket.on('test', function() {
        console.log('test received');
    });

    socket.on('joinQueue', function() {
        console.log('server joinQueue');

        // Join player to queue
        server.players.push(socket.id);

        // Pair players
        assocPlayersToMatch();

    });

    socket.on('sendInfoTeam', function(data) {
        //console.log('server sendInfoTeam');

        var assigned = false;
        teams[socket.id] = new TeamMirror(socket.id);
        for (var i = 0; i < balls.length; i++) {
            // if there is a ball with my opponent's socket
            if (balls[i].socketLocal != null) {
                if (balls[i].socketLocal == data[SOCKET]) {
                    // add this socket to complete the BallMirror instance
                    balls[i].socketVisitor = socket.id;
                    assigned = true;
                    break;
                }
            } else if (balls[i].socketVisitor != null) {
                if (balls[i].socketVisitor == data[SOCKET]) {
                    balls[i].socketLocal = socket.id;
                    assigned = true;
                    break;
                }
            }
        }
        // if there isn't, create new BallMirror with this socket
        if (assigned == false) {
            var ball = new BallMirror();
            if (data[DATA].side == 0) {
                ball.socketLocal = socket.id;
            } else {
                ball.socketVisitor = socket.id;
            }
            balls.push(ball);
        }
        sendInfoTeam(data);
    });

    socket.on('sendPlayersInfo', function(data) {
        if (teams.hasOwnProperty(socket.id)) {
            console.log('server sendPlayersInfo');

            if (teams[socket.id].players.length == 0) {
                teams[socket.id].createPlayerMirrors(data[DATA]);
            } else {
                teams[socket.id].updatePlayerMirrors(data[DATA]);
                for (var i = 0; i < balls.length; i++) {
                    if (balls[i].socketLocal == socket.id || balls[i].socketVisitor == socket.id) {
                        if (balls[i].flag == true) {
                            balls[i].setProperties(data[BALL].x, data[BALL].y, data[BALL].velX, data[BALL].velY);

                            var newOwner = null;
                            console.log("OWNER: " + data[BALL].owned);
                            if (data[BALL].owned == false) {
                                console.log("-----------------\nPlayers sLocal:");
                                console.log(teams[balls[i].socketLocal].players);
                                console.log("-----------------");
                                newOwner = balls[i].updateOwner(teams[balls[i].socketLocal].players.concat(teams[balls[i].socketVisitor].players));
                                console.log("New owner: " + newOwner);

                                var dic = {
                                    newOwner: newOwner,
                                    x: balls[i].x,
                                    y: balls[i].y
                                };

                                io.to(balls[i].socketLocal).emit('ballProperties', dic);
                                io.to(balls[i].socketVisitor).emit('ballProperties', dic);
                            }
                            balls[i].flag = false;
                            break;
                        } else {
                            balls[i].flag = true;
                        }
                    }
                }
            }
            sendPlayersInfo(data);
        }
    });

    socket.on('moveBall', function (speed) {
        for (var i = 0; i < balls.length; i++) {
            if (balls[i].socketLocal == socket.id) {
                io.to(balls[i].socketVisitor).emit('moveBall', speed);
            } else if (balls[i].socketVisitor == socket.id) {
                io.to(balls[i].socketLocal).emit('moveBall', speed);
            }
        }
    });

    socket.on('endMatch', function() {
        if (teams.hasOwnProperty(socket.id)) {
            delete teams[socket.id];
        }
    });

    socket.on('sendEmoji', function(data) {
        console.log('server sendEmoji');

        sendEmoji(data);
    });

});

function assocPlayersToMatch() {
    var disconnected = [];
    console.log('assocPlayersToMatch');

    for (var i = 0; i < server.players.length; i++) {
        if (io.sockets.connected[server.players[i]]) {
            for (var j = i+1; j < server.players.length; j++) {
                if (io.sockets.connected[server.players[i]]) {
                    joinToMatch(i, j);
                    popPlayer(j);
                    popPlayers(disconnected);
                    popPlayer(i);
                    return;
                } else {
                    disconnected.push(j);
                }
            }
        } else {
            // Delete disconnected player
            disconnected.push(i);
        }
    }
    // Delete the disconnected players
    popPlayers(disconnected);
}

function joinToMatch(index1, index2) {
    console.log('joinToMatch');
    var id = new Date().getTime();
    var data = [];
    data.push(server.players[index2]);
    data.push({
        side: 0,
        matchID: id
    });
    io.to(server.players[index1]).emit('startMatch', data);
    data = [];
    data.push(server.players[index1]);
    data.push({
        side: 1,
        matchID: id
    });
    io.to(server.players[index2]).emit('startMatch', data);
    //io.emit('startMatch', server.players[index2]); // data server.players[index1]
}

function sendInfoTeam(info) {
    console.log('server to client sendInfoTeam');
    io.to(info[SOCKET]).emit('getOppInfo', info[DATA]);
}

function sendPlayersInfo(info) {
    //console.log('server to client sendPlayersInfo');
    io.to(info[SOCKET]).emit('getPlayersInfo', info[DATA]);
}

function sendEmoji(info) {
    console.log('server to client sendEmoji: ' + info[DATA]);
    io.to(info[SOCKET]).emit('getEmoji', info[DATA]);
}

function popPlayers (indexes) {
    for (var i = indexes.length - 1; i >= 0; i--) {
        popPlayer(indexes[i]);
    }
}

function popPlayer (index) {
    server.players.splice(index, 1);
}

function randomInt (low, high) {
    return Math.floor(Math.random() * (high - low) + low);
}
