// Constants
require('./PlayerMirror');

function TeamMirror (socket) {

    this.socket = socket;
    this.players = [];

    this.createPlayerMirrors = function (data) {
        for (var i = 0; i < data.length; i++) {
            this.players.push(new PlayerMirror(data[i].x, data[i].y));
        }
    };

    this.updatePlayerMirrors = function(data) {
        for (var i = 0; i < data.length; i++) {
            this.players[i].x = data[i].x;
            this.players[i].y = data[i].y;
        }
    };

    return {
        socket: this.socket,
        players: this.players,
        createPlayerMirrors: this.createPlayerMirrors,
        updatePlayerMirrors: this.updatePlayerMirrors
    }
}

module.exports = TeamMirror;