// Constants

// Class that represents the ball
PlayerMirror = function (x, y) {

    this.x = x;
    this.y = y;

    return {
        x: this.x,
        y: this.y
    }
};

module.exports = PlayerMirror;