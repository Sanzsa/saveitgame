// Constants
var maxCatchVelocity = 160;

// Class that represents the ball
BallMirror = function () {

    this.socketLocal = null;
    this.socketVisitor = null;

    this.x = 0;
    this.y = 0;

    this.velocityX = 0;
    this.velocityY = 0;

    this.flag = false;

    this.setProperties = function (x, y, velX, velY) {
        this.x = x;
        this.y = y;
        this.velocityX = velX;
        this.velocityY = velY;
    };

    /* *****************************************
     * updateOwner
     *
     * Updates the owner of the ball.
     *
     * Params
     *    players: array of players
     * *****************************************/
    this.updateOwner = function (players) {
        console.log("DISTANCIAS PLAYERS - BALL:");
        console.log(this.module(this.velocityX, this.velocityY) + "<=" + maxCatchVelocity);
        if ((this.module(this.velocityX, this.velocityY) <= maxCatchVelocity)) {
            for (var i = 0; i < players.length; i++) {
                console.log("--------------\nx:" + players[i].x + " - y:" + players[i].y);
                console.log("ballX:" + this.x + " - ballY:" + this.y + "\n--------------");
                console.log(this.distanceTo(players[i].x, players[i].y));
                if (this.distanceTo(players[i].x, players[i].y) <= 15) {
                    // if (this.owner != null) {
                    //     this.owner.setBallControl(null);
                    // }
                    return i;
                }
            }
        }
        return null;
    };

    this.module = function (x, y) {
        return Math.sqrt((x * x ) + (y * y));
    };

    this.distanceTo = function (x, y) {
        return this.module(this.x-x,this.y-y);
    };

    return {
        x: this.x,
        y: this.y,
        velocityX: this.velocityX,
        velocityY: this.velocityY,

        setProperties: this.setProperties,
        updateOwner: this.updateOwner,
        module: this.module,
        distanceTo: this.distanceTo
    }
};

module.exports = BallMirror;